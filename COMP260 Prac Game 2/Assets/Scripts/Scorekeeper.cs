﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorekeeper : MonoBehaviour {

    static private Scorekeeper instance;
    public int pointsPerGoal = 1;
    private int[] score = new int[2];

    static public Scorekeeper Instance
    {
        get { return instance; }
    }

	// Use this for initialization
	void Start ()
    {
		if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("More than one Scorekeeper exists in the scene");
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnScoreGoal (int player)
    {
        score[player] += pointsPerGoal;
    }
}
