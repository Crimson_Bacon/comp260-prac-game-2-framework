﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class PuckControl : MonoBehaviour
{
    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;
    public Transform startingPos;
    private new AudioSource audio;
    private new Rigidbody rigidbody;


	// Use this for initialization
	void Start ()
    {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        ResetPosition();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        // check what we have hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            // hit the paddle
            audio.PlayOneShot(paddleCollideClip);
        }
        else
        {
            // hit something else
            audio.PlayOneShot(wallCollideClip);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        Debug.Log("Collision Stay: " + collision.gameObject.name);
    }

    void OnCollisionExit(Collision collision)
    {
        Debug.Log("Collision Exit: " + collision.gameObject.name);
    }

    public void ResetPosition()
    {
        rigidbody.MovePosition(startingPos.position);
        rigidbody.velocity = Vector3.zero;
    }
}
