﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class Goal : MonoBehaviour {

    public AudioClip scoreClip;
    public int player;
    private new AudioSource audio;

	// Use this for initialization
	void Start ()
    {
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnTriggerEnter(Collider collider)
    {
        audio.PlayOneShot(scoreClip);
        
        PuckControl puck = collider.gameObject.GetComponent<PuckControl>();
        puck.ResetPosition();
    }
}
