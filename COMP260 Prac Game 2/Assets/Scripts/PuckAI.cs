﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuckAI : MonoBehaviour
{
    public float responseTime = 3;
    public float speed = 100;
    public GameObject puck;
    private Rigidbody rigidbody;
    private float cooldownTime = 0;
    private bool cooldown = true;

	// Use this for initialization
	void Start ()
    {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        cooldownTime += Time.fixedDeltaTime;
        if (cooldownTime >= responseTime)
        {
            cooldown = false;
        }
		if (cooldown == false)
        {
            Vector3 direction = puck.transform.position - transform.position;
            rigidbody.AddForce(direction.normalized * speed);
            cooldown = true;
            cooldownTime = 0;
        }
	}
}
